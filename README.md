## Alfred Examples Library

![Alfred](https://docs.alfred-cms.com/alfred-logo.svg)

---

Purpose of this repository is to create a centralised place with all the useful examples of Alfred's functionalities implementation from real projects. It contains the most common cases of project needs with ready-to-use code snippets.

Feel free to go through all the folders, read the instructions and implement the functionalities/modules/widget/whatever in your own project!