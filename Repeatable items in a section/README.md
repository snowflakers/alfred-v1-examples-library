# Repeatable items in a section
---

## Introduction
This tutorial will help you to understand how and when you should create a custom microcomponent that will allow Alfred users to add repeatable items that have the same data structure.

**The general rule you should follow is**: if in a section there are elements that have repeated data set, then you should create a custom microcomponent.

Please take a look at a few examples below. Please consider examples of sections (mentioned at the end of this tutorial) that should NOT be created using custom microcomponent too. **It is important to recognise a case when you need a custom microcomponent and when not.**

## Examples of sections when a custom microcomponent is the right choice

- Logos
- Company history
- Blocks with some image/text/link
- Testimonial slider
- Company Process with some described steps
- Offices
- Services
- Featured Products

## Example 1
![Example of a history section](https://docs.alfred-cms.com/repeatable-items-section1.png)

This section presents 2 main elements/fields:

- headline
- list of **history items**

Each history item is repeatable - has the same data set within in. It is:

- image
- year
- text

A single history item is not a database record - in other words, there is no need to create a page that you should go to after clicking some of the history item. It is just a repeatable item that should only exist in this section and nowhere else.

If you recognise this kind of repeated data elements, it is a clear sign you should create a **custom microcomponent**. See below how to do it.

### Example 1, step 1: create a new custom microcomponent
Go to `cms-backend/resources/cmsassets/_ng/templates/templates.tpl.html` and add a new microcomponent (can be just added at the bottom of the file but it does not matter really):

```
<script type="text/ng-template" id="component-history-items">
    <label>
        <b>{{component.label}}</b>
    </label>
    <div class="area" ui-sortable="page.content.sortableOptions" ng-model="section.components[component.key]">
        <div class="panel panel-warning allow-sort" ng-repeat="item in section.components[component.key]">
            <div class="panel-heading">
                Item {{ $index+1 }}

                <button
                    type="button"
                    class="btn btn-danger btn-xs pull-right"
                    ng-click="section.components[component.key].splice($index, 1)">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>

                <span class="pull-right glyphicon glyphicon-move myHandle"></span>
            </div>

            <div class="panel-body">
                <image-microcomponent
                    label="'Image'"
                    key="'image'"
                    data="item">
                </image-microcomponent>

                <div class="input-group">
                    <span class="input-group-addon">Year</span>
                    <input type="text" class="form-control" ng-model="item.year">
                </div>

                <div class="input-group">
                    <span class="input-group-addon">Text</span>
                    <textarea ui-tinymce="$root.tinymce.default" ng-model="item.text"></textarea>
                </div>
            </div>
        </div>
    </div>
    <button
        type="button"
        class="btn btn-warning"
        style="margin-bottom: 70px;"
        ng-click="section.components[component.key].push({image: '', year: '', text: ''}) || (section.components[component.key] = [{image: '', year: '', text: ''}])"
        >
        Add new item
    </button>
</script>
```

Remember that in a custom microcomponent template (that is visible in a section in Alfred only!) you have a total freedom. It is just an HTML with a little addition of JS thanks to that you can add multiple elements, remove them, re-order etc.

### Example 1, step 2: use the custom microcomponent in your section
Example of a section configuration (create a section JSON file in `cms-backend/resources/views/Website/Sections/jsons`:

```
{
    "name": "History section",
    "type": [
        "page",
        "homepage"
    ],
    "group": "special",
    "components": {
        "settings": {
            "order": 1,
            "type": "section-modal",
            "modal_type": "section-settings",
            "label": "Settings"
        },
        "headline": {
            "order": 2,
            "type": "input",
            "label": "Headline",
            "cms_width": 9
        },
        "headline_colour": {
            "order": 3,
            "type": "text-colours",
            "label": "Headline Colour",
            "cms_width": 3
        },
        "history_items": {
            "order": 4,
            "type": "history-items",
            "label": "History items"
        },
        "section_anchor":{
            "order": 5,
            "label": "Section ID",
            "type": "input"
        }
    }
}
```

Now if you create a blade template file at `cms-backend/resources/views/Website/Sections/`, in Alfred, you will be able to select that section and you should see it as below:

![Example of a history section in Alfred](https://docs.alfred-cms.com/repeatable-items-section2.png)

Alfred Users are able to add as many history items as they want and each single history element has the same data set (available fields within a single item).

### Example 1, step 3: create the section visible on front page
Below you can see an example how to access the list of history items. It is simple!

```
<section
    class="
        section 
        @include('Website.Sections.columns-sections-partials.section-settings')
    "
    @include('Website.Templates.section-anchor-id')>

    @include('Website.Sections.columns-sections-partials.background')

    @include('Website.Sections.columns-sections-partials.overlay')

    ... your HTML code here

    @if (!empty($components->history_items))
        @foreach ($components->history_items as $item)
            <article>
                @if (!empty($item->image))
                    {!! Imgix::imageCustom
                        (
                            $item, 
                            'image',
                            'image_alt',
                            'semi-full-width-image',
                            '',
                            'image'
                        ) 
                    !!}
                @endif

                @if (!empty($item->year))
                    <h2>
                        {{ $item->year }}
                    </h2>
                @endif

                @if (!empty($item->text))
                    <div class="content">
                        {!! $item->text !!}
                    </div>
                @endif
            </article>
        @endforeach
    @endif
</section>
```

## Important: remember about `<image>` and `<buttons>` microcomponent
If you create a new custom microcomponent, remember that you can use over there other microcomponents that are represented by directives. The 2 most popular directives you can use are:

- `<image>`
- `<buttons>`

Example of image (**it was used in that history section above in this tutorial**):

```
    <image-microcomponent
        label="'Image'"
        key="'image'"
        data="item">
    </image-microcomponent>
```

Example of buttons:

```
<buttons data="item.btns"></buttons>
```

## Example 2
![Example of a About Us section](https://docs.alfred-cms.com/repeatable-items-section3.png)

This kind of section is another good example where a custom microcomponent could be used. On the right side you can see a list of repeated items. You could create a section that would consist of:

- Section Settings
- Headline
- Text
- Buttons
- **Features - custom microcomponent**

In the **Features** microcomponent, single item could have following fields to edit:

- Headline
- Text

## Example 3
![Example of a Featured Products section](https://docs.alfred-cms.com/repeatable-items-section4.png)

**This example of section is quite specific.** As you can see, there are repeatable items - products - and in order to make this section editable, you could create a custom microcomponent where a single item would contain following options/fields to edit:

- Product Link URL
- Image
- Price from
- Logo (image)
- Text

However, as **the products represent real pages in Alfred**, better way to make this section manageable would be to create a custom microcomponent where in a single item Alfred User would just select a page from products module and all the single elements (product link, image, logo, text, price etc.) would come there automatically from those single pages. It is extrenely helpful for Alfred Users in case of that kind of element such as product card exists on the website in many other places/sections. This way, you could centralise the fields management (image, price from, text, logo etc.) in single product pages (for example in the Lead tab) and then just display all the info in a section.

Below you can see an example of a custom microcomponent where a single item would rely on `include-filter` microcomponent where you can read about in [Alfred Documentation](https://docs.alfred-cms.com/micro-components). Users would easily select a page and all the info in that section would be pulled from that page.

### Example 3, step 1: create a new custom microcomponent

```
<script type="text/ng-template" id="component-featured-products">
    <label>
        <b>{{component.label}}</b>
    </label>

    <div 
        style="margin-bottom: 20px" 
        ui-sortable="page.content.sortableOptions" 
        ng-model="section.components[component.key]">
        <div class="panel panel-warning allow-sort" ng-repeat="product in section.components[component.key]">
            <div class="panel-heading">
                Product {{ $index+1 }}

                <button
                    type="button"
                    class="btn btn-danger btn-xs pull-right"
                    ng-click="section.components[component.key].splice($index, 1)">
                    <span class="glyphicon glyphicon-remove"></span>
                </button>

                <span class="pull-right glyphicon glyphicon-move myHandle"></span>
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <label>
                        <b>Product</b>
                    </label>
            
                    <input 
                        placeholder="Type here the name of product to select it" 
                        autocompleter class="form-control"
                        data-type="products" 
                        data-include="product.page" 
                        ng-model="product.page.title">

                    <small ng-if="product.page.id">
                        <a ng-click="product.page = {}" href>remove</a>
                    </small>
                </div>
            </div>
        </div>
    </div>
    <button
        type="button"
        class="btn btn-warning"
        style="margin-bottom: 70px;"
        ng-click="section.components[component.key].push({product: {}}) || (section.components[component.key] = [{product: {}}])"
    >
        Add new product
    </button>
</script>
```

### Example 3, step 2: use the custom microcomponent in your section
Example of a section configuration (create a section JSON file in `cms-backend/resources/views/Website/Sections/jsons`:

```
{
    "name": "Featured products",
    "type": [
        "page",
        "homepage"
    ],
    "group": "special",
    "components": {
        "settings": {
            "order": 1,
            "type": "section-modal",
            "modal_type": "section-settings",
            "label": "Settings"
        },
        "headline": {
            "order": 2,
            "type": "input",
            "label": "Headline",
            "cms_width": 9
        },
        "headline_colour": {
            "order": 3,
            "label": "Headline colour",
            "type": "text-colours",
            "cms_width": 3
        },
        "cta_btns": {
            "order": 4,
            "type": "btns",
            "label": "CTA buttons"
        },
        "products": {
            "order": 5,
            "label": "Featured products",
            "type": "featured-products"
        },
        "section_anchor": {
            "order": 6,
            "label": "Section ID",
            "type": "input"
        }
    }
}
```

Now if you create a blade template file at `cms-backend/resources/views/Website/Sections/`, in Alfred, you will be able to select that section and you should see it as below:

![Example of a Featured Products in Alfred](https://docs.alfred-cms.com/repeatable-items-section5.png)


### Example 3, step 3: create the section visible on front page
Below you can see an example how to access the list of products. It is simple!

```
<section
    class="
        section 
        @include('Website.Sections.columns-sections-partials.section-settings')
    "
    @include('Website.Templates.section-anchor-id')
>
    @include('Website.Sections.columns-sections-partials.background')
    @include('Website.Sections.columns-sections-partials.overlay')

    <div class="container-medium d-md-flex justify-content-between align-items-center mb-20em">
        @if (!empty($components->headline))
            <h2 
                class="
                    headline-2 
                    {!! HTML::fontColour($components, 'headline_colour') !!}
                ">
                {{ $components->headline }}
            </h2>
        @endif

        @if (!empty($components->cta_btns))
            @foreach($components->cta_btns as $btn)
                {!! HTML::button($btn) !!}
            @endforeach
        @endif
    </div>

    @if (!empty($components->products))
        @php
            $products = \WebsiteBase::getSelectedPages($components, 'products', 'products');
        @endphp

        @if (!empty($products) && !$products->isEmpty())
            <div class="@include('Website.Sections.columns-sections-partials.container-settings')">
                <div class="swiper-container animation-fadeInRight js-swiper overflow-visible" data-swiper="featured-products">

                    <div class="swiper-wrapper ">
                        @foreach ($products as $productPage)
                            @include('Website.Sections.partials.product-card-link')
                        @endforeach
                    </div>

                    <div class="tile-pagination-wrapper">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="tile-pagination__arrow-prev featured-products-prev">
                                <svg role="img" aria-labelledby="scroll-arrow" width="32px" height="16px">
                                    <title id="scroll-arrow">Scroll down</title>
                                    <use xlink:href="/images/icons.svg#icon-arrow"></use>
                                </svg>
                            </div>
                            <div class="tile-pagination featured-products-pagination"></div>
                            <div class="tile-pagination__arrow-next featured-products-next">
                                <svg role="img" aria-labelledby="scroll-arrow" width="32px" height="16px">
                                    <title id="scroll-arrow">Scroll down</title>
                                    <use xlink:href="/images/icons.svg#icon-arrow"></use>
                                </svg>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endif
</section>
```

As you can see, each single product card has got a separate blade template (`@include('Website.Sections.partials.product-card-link')`) to manage it easily. Below you can see the `product-card-link.blade.php` file:

```
<a 
    class="swiper-slide tile tile--border tile--hover tile--product mw-424 bg-white" 
    href="{{ $root . $productPage->slug }}" 
    title="{{ $productPage->title }}">

    @if (!empty($productPage->lead->image))
        <figure class="tile__image">
            {!! Imgix::imageCustom
                (
                    $productPage->lead, 
                    'image', /* key of $components object under which there is path to image saved */
                    'image_alt', /* alt text (can be any other key from $components) */
                    'semi-full-width-image', /* key from config/srcset-settings.php -> section_settings */
                    'tile__large-image', /* specify CSS class added to <img> if needed */
                    'image'
                ) 
            !!}
        </figure>
    @endif

    <div class="tile__content tile__content--p-small">
        <h3 class="tile__headline color-black">
            {{ $productPage->title }}
        </h3>

        <div>
            @if (!empty($productPage->price_from))
                <span class="tile__small-tile tile--product bg-orange rounded-corners font-bold color-white">
                    From £{{ $productPage->price_from }}
                </span>
            @endif

            @if (!empty($productPage->lead->brand_image))
                @php
                    $imageSizes = HTML::getImageSizes($productPage->lead->brand_image);
                @endphp

                <img 
                    class="tile__small-img ml-10em lazy" 
                    data-src="{{ $imgixDomain . $productPage->lead->brand_image }}" 
                    width
                    height="20"
                    alt="{{ !empty($productPage->lead->brand_image_alt) ? $productPage->lead->brand_image_alt : '' }}">
            @endif
        </div>

        @if (!empty($productPage->lead->features))
            <span class="d-block mt-15em mb-5em color-black font-bold">Perfect for</span>
            <ul class="tile__list">
                @foreach ($productPage->lead->features as $feature)
                    @continue(empty($feature->text))

                    <li class="d-flex align-items-center color-black ">
                        <svg class="fill-blue mr-10em" width="16px" height="16px" aria-label="hidden">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                href="/images/icons.svg#icon-check-circle">
                            </use>
                        </svg>
                        {{ $feature->text }}
                    </li>
                @endforeach
            </ul>
        @endif
    </div>
</a>
```

Easy!

## Example 4 - logo section

![Example of a logo section](https://docs.alfred-cms.com/repeatable-items-section6.png)

### Example 4, step 1 - create a custom microcomponent
This microcomponent should allow Alfred Users to add as many logos as they want. Each single logo should be just a simple image upload and eventually a field to enter an URL.

```
<script type="text/ng-template" id="component-logos">
    <div style="margin-bottom: 20px">
        <label>
            <b>{{ component.label }}</b>:
        </label>

        <div ui-sortable ng-model="section.components[component.key]">
            <div class="panel panel-primary" ng-repeat="logo in section.components[component.key]">
                <div class="panel-heading">
                    Logo {{ $index+1 }}
                    <span class="glyphicon glyphicon-move"></span>
                </div>
                <div class="panel-body">
                    <image-microcomponent
                        label="'Logo image'"
                        key="'image'"
                        data="logo">
                    </image-microcomponent>

                    <div class="input-group">
                        <span class="input-group-addon">URL</span>
                        <input type="text" class="form-control" ng-model="logo.url">
                    </div>
                </div>

                <div class="panel-footer text-right">
                    <button
                        type="button"
                        class="btn btn-danger"
                        ng-click="section.components[component.key].splice(section.components[component.key].indexOf(logo), 1)"
                        >
                        Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
    <button
        type="button"
        class="btn btn-primary"
        ng-click="section.components[component.key].push({image: '', image_alt: '', url: ''}) || (section.components[component.key] = [{image: '', image_alt: '', url: ''}])"
        style="margin-bottom: 30px">
        Add logo
    </button>
</script>
```

Then you could follow all the other examples above and create a new section where one of the fields would be that **logos** microcomponent. Then in the blade template for the front page, you would iterate through the logos and display them.

## Examples of sections when a custom microcomponent **is not the right choice**

Below you can find a list of section types where it is better to have a different approach. Go to specific descriptions of those examples in order to understand it better.

- **Meet the team** - please [read more about it here](https://bitbucket.org/snowflakers/alfred-v1-examples-library/src/master/Meet%20the%20team%20section/)
- **Latest News / Latest Blog / Latest Case Studies etc.** or **Related News/Blog/Products etc.** - please [read more about it here](https://bitbucket.org/snowflakers/alfred-v1-examples-library/src/master/Latest%20content%20section%20(Latest%20News%2C%20Insights%2C%20Case%20Studies%20etc.)/) how to implement it
