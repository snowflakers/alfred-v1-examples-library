# Notification bar
---

## What is it?
It is helpful Alfred's functionality that displays a notification bar on the website. Content of the bar can be set up globally through the Partials module but also overriden on any page (in any module where you can install this widget - see point 9 of the Installation section).

![Example of notification bar implementation](https://docs.alfred-cms.com/alfred-notification-bar-example1.png)

![Example of notification bar implementation 2](https://docs.alfred-cms.com/alfred-notification-bar-example2.png)

![Example of notification bar implementation 3](https://docs.alfred-cms.com/alfred-notification-bar-example3.png)

![Notification bar management in Partials](https://docs.alfred-cms.com/alfred-notification-bar-example4.png)

---

## Installation
Please go to dedicated [Notification Bar](https://bitbucket.org/snowflakers/alfred-v1-notification-bar) repository where you can find the source code and instructions.