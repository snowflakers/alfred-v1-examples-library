# Traffic Source Tracking
---

## What is it?
**This feature creates a cookie** on the front-end side that contains an information about the traffic source (for example that user has arrived on the site from a Google campaign). Then, the details are being saved in Alfred Forms on form submission if needed. This can be saved in any other destination of course!

Below an example of a form submission sent by user who came to a website from Google:

![Example of traffic source info](https://docs.alfred-cms.com/alfred-source-traffic-tracking.png)

Please remember that it works only on a website uder **https://**.

---

## Installation

### Step 1
Create a file **traffic-source-tracking.blade.php** in the following location `cms-backend/resources/views/Website/Layout/partials/traffic-source-tracking.blade.php` and paste the following code:

```
<script>
  function crumbleCookie(a) {
    for (var d = document.cookie.split(";"), c = {}, b = 0; b < d.length; b++) {
        var e = d[b].substring(0, d[b].indexOf("=")).trim(),
            i = d[b].substring(d[b].indexOf("=") + 1, d[b].length).trim();
        c[e] = i
    }
    if (a) return c[a] ? c[a] : null;
    return c;
}

function bakeCookie(a, d, c, b, e, i) {
    var j = new Date;
    j.setTime(j.getTime());
    c && (c *= 864E5);
    j = new Date(j.getTime() + c);
    document.cookie = a + "=" + escape(d) + (c ? ";expires=" + j.toGMTString() : "") + (b ? ";path=" + b : "") + (e ? ";domain=" + e : "") + (i ? ";secure" : "")
}

function writeLogic(n) {
    var a = getTrafficSource(n);
    
    a = a.replace(/\|{2,}/g, "|");
    a = a.replace(/^\|/, "");
    a = unescape(a);
    
    bakeCookie(n, a, 90, "/", "", true)
};

function getParam(s, q) {
  try{
      var match = s.match('[?&]' + q + '=([^&]+)');
      return match ? match[1] : '';
  } catch(e){
    return '';    
  }
}

function calculateTrafficSource() {
  var source='', medium='', campaign='', term='', content='';
  var search_engines = [['bing', 'q'], ['google', 'q'], ['yahoo', 'q'], ['baidu', 'q'], ['yandex', 'q'], ['ask', 'q']];	
  var ref = document.referrer;
  ref = ref.substr(ref.indexOf('//')+2);
  ref_domain = ref;
  ref_path = '/';
  ref_search = '';
  

  var url_search = document.location.search;
  
  if(url_search.indexOf('utm_source') > -1) {
    source   = getParam(url_search, 'utm_source');
    medium   = getParam(url_search, 'utm_medium');
    campaign = getParam(url_search, 'utm_campaign');
    term     = getParam(url_search, 'utm_term');
    content  = getParam(url_search, 'utm_content');
  } 
  else if (getParam(url_search, 'gclid')) {
    source = 'google';
    medium = 'cpc';
    campaign = '(not set)';
  } 
  else if(ref) {
 
    if (ref.indexOf('/') > -1) {
      ref_domain = ref.substr(0,ref.indexOf('/'));
      ref_path = ref.substr(ref.indexOf('/'));
      if (ref_path.indexOf('?') > -1) {
        ref_search = ref_path.substr(ref_path.indexOf('?')+1);
        ref_path = ref_path.substr(0, ref_path.indexOf('?'));
      }
    }
    medium = 'referral';
    source = ref_domain;                      

    for (var i=0; i<search_engines.length; i++){
      if(ref_domain.indexOf(search_engines[i][0]) > -1){
        medium = 'organic';
        source = search_engines[i][0];
        term = getParam(ref_search, search_engines[i][1]) || '(not provided)';
        break;
      } 
    }
  }
 
  return {
    'source'  : source,
    'medium'  : medium,
    'campaign': campaign,
    'term'    : term,
    'content' : content
  };
}

function getTrafficSource(cookieName) {
  var trafficSources = calculateTrafficSource();
  var source = trafficSources.source.length === 0 ? 'direct' : trafficSources.source;
  var medium = trafficSources.medium.length === 0 ? 'none' : trafficSources.medium;
  var campaign = trafficSources.campaign.length === 0 ? 'direct' : trafficSources.campaign;

  if(medium === 'referral') {
      campaign = '';
  }
  var rightNow = new Date();
  var value = 'source='   + source +
              '&medium='  + medium +
              '&campaign='+ campaign +
              '&term='    + trafficSources.term +
              '&content=' + trafficSources.content +
              '&date='    + rightNow.toISOString().slice(0,10).replace(/-/g,"");
    return value;
}

(function(){
	var date = new Date();	
	var fr_date = date.getUTCFullYear().toString() + ((date.getUTCMonth() < 9) ? '0' + (date.getUTCMonth()+1).toString() : (date.getUTCMonth()+1).toString()) + ((date.getUTCDate() < 10) ? '0' + date.getUTCDate().toString() : date.getUTCDate().toString());
	var session = crumbleCookie()['firstVisitSession'];

	if (typeof session == 'undefined') {
		writeLogic('firstVisitSession');		
	}
})();
</script>
```

### Step 2
In the ```cms-backend/resources/views/Website/Layout/partials/scripts-body.blade.php``` file, include the above file as per example below:

```
@include('Website.Layout.partials.traffic-source-tracking')

...the rest of the existing code
```

### Step 3
If you want to pass the traffic source info into the Alfred Form submissions, in the `cms-backend/app/Website/Modules/Forms/Controllers/FormController.php` create a method:

```
    private function getTrafficSourceDetails(): array
    {
        $fieldsToReturn = [];
        $cookieValue = !empty($_COOKIE['firstVisitSession']) ? $_COOKIE['firstVisitSession'] : null;
        $desiredParams = ['source', 'medium', 'campaign'];

        parse_str($cookieValue, $params);

        foreach ($desiredParams as $param) {
            if (!empty($params[ $param ])) {
                $fieldsToReturn[] = [
                    'display_name' => $param,
                    'value' => $params[ $param ]
                ];
            }
        }

        return $fieldsToReturn;
    }
```

and then in the same controller, in each method responsible for form submission (such as `sendForm()`), find ```all_fields_from_form``` and merge the existing array with the traffic source info array. Example:

```
    'all_fields_from_form' => array_merge($allFields, $this->getTrafficSourceDetails()),
```

That's everything what you need to do. Now, in order to test it out, you have to get it deployed and visit your site from (for example) Google search results via a campaign link and submit a form on the website. The Source/Medium/Campaign details should be saved in a form submission and be visible in Alfred.

---

## Customisation
Remember that if you need to save the traffic source info in a different than Alfred Form place, you just need to read the ```$_COOKIE``` and find `source`, `medium`, `campaign` to get their values and save in a desired destination.