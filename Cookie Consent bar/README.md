# Cookie Consent Bar
---

## Introduction
Because of the GDPR law, websites are obliged to present information about cookie use. Users need to have an ability to opt-in or opt-out. This cookie consent bar is very simple solution that only presents the info (that is manageable via Partials in Alfred) and user is only able to agree the cookie policy by cliking a button.

![Example of cookie bar](https://docs.alfred-cms.com/cookie-bar1.png)

![Example of cookie bar 2](https://docs.alfred-cms.com/cookie-bar2.png)

![Example of cookie bar 3](https://docs.alfred-cms.com/cookie-bar3.png)

---

## What you need to do to implement it in your project

### Step 1
In `cms-backend/resources/cmsassets/_ng/Partials/index.tpl.html` add a new tab called **Cookie Consent bar**:

```
    <tab heading="Cookie Consent bar">
        <div class="panel-body">
            <div class="input-group">
                <span class="input-group-addon">Text</span>
                <textarea 
                    class="form-control"
                    ui-tinymce="$root.tinymce.basic"
                    ng-model="partials.settings.cookies_text">
                </textarea>
            </div>
        </div>
    </tab>
```

Thanks to this, it is now possible to edit the cookie consent bar text/content.

### Step 2
Now you need to create a blade template responsible for the bar displaying on the front-end page. The recommend place to add it is the folder `cms-backend/resources/views/Website/Layout/partials`. Add a new file over there under this name: `cookie-notification.blade.php`. Below you can find an example of HTML but of course **feel free to adjust it to your project**:

```
@if (!empty($settings['cookies_text']))
    <div class="cookie-bar js-cookie-bar">
        <p>
            {!! $settings['cookies_text'] !!}
        </p> 

        <button 
            type="button" 
            title="Consent to our cookie policy"
            class="js-close-cookie-bar-btn">
            OK
        </button>
    </div>
@endif
```

### Step 3
Include the template in your master layout blade, in the `cms-backend/resources/views/Website/Layout/default.blade.php`, in an appropriate file:

```
@include('Website.Layout.partials.cookie-notification') 
```

That's it! Now you need some front-end code and if you want to use from the ready example too, see below.

## Frontend
Below you can find an example of the JS module that is responsible for displaying the notification bar and triggering the action of closing it if user decides to hide the bar.

### Javascript

### scripts/components/cookie-bar.js

```
'use strict';

import { isCookieSet, setCookie } from '../utils/cookie-actions';

const COOKIE_NAME = 'cookiePolicyConsent';
const closeButton = document.querySelector('.js-close-cookie-bar-btn');
const bar = document.querySelector('.js-cookie-bar');

const show = () => {
    if (!(isCookieSet({ name: COOKIE_NAME }))) {
        cookieLayer.classList.add('cookie-bar--show');
    }
};

const closeBar = () => {
    setCookie({
        name: COOKIE_NAME,
        val: true
    });
    bar.classList.remove('cookie-bar--show');

    return false;
};

const initNotification = () => {
    if (bar) {
        show();
    }

    if (closeButton) {
        closeButton.addEventListener('click', closeBar);
    }
};

export {
    initNotification
};

```

Now you need to call `initNotification();` method somewhere in your main JS entrypoint such as app.js etc.

### scripts/utils/cookie-actions.js - this may be useful too

```
'use strict';

const setCookie = ({ name, val, days }) => {
    let expires;

    if (days) {
        let data = new Date();
        data.setTime(data.getTime() + days * 24 * 60 * 60 * 1000);
        expires = "; expires=" + data.toGMTString();
    } else {
        expires = "";
    }

    document.cookie = name + "=" + val + expires + "; path=/";
};

const isCookieSet = ({ name }) => {
    if (document.cookie !== "") {
        const cookies = document.cookie.split("; ");

        for (let i = 0; i < cookies.length; i++) {
            let cookieName = cookies[i].split("=")[0];
            let cookieVal = cookies[i].split("=")[1];

            if (cookieName === name) {
                return decodeURI(cookieVal);
            }
        }
    }
};

export { setCookie, isCookieSet };
```

### styles/components/_components/_cookie-bar.scss
It is only an example. Feel free to adjust it.

```
.cookie-bar {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 1000;
    transform: translateY(100px);
    opacity: 0;
    will-change: transform, opacity;
    transition: opacity .4s,transform .4s;

    &.cookie-bar--show {
        opacity: 1;
        transform: translateY(0);
    }
}

```