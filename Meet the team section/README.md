# Meet the team section
---

## Introduction
Section with team members - very common case. Sometimes as a simple list, sometimes as a carousel, sometimes as a slider or even with a functionality to open some person's bio in a popup. This tutorial will definietly help you to understand how to create such a section quickly.

The best way to achieve this, is to use **Components module** where you can find the ready **People** component type. Records (people) added over there will have a representation in a separate database table (`component_people`) thanks to which in case of a need to display the people in different part of application (for example to display managers in some ecommerce project), it will be completely easy to get it done.

![Example of implementation](https://docs.alfred-cms.com/section-meet-the-team-example1.png)

## What you need to do
### Step 1
Go to Alfred --> Components module and create a new component using **People** type. Name it how you want.

### Step 2
If you click the button to add a new person, you will get a form to fill out. All the fields are configurable in `cms-backend/cmsassets/_ng/Components/people-create.tpl.html` and `cms-backend/cmsassets/_ng/Components/people-edit.tpl.html`. **Please edit both files** and create exactly the same fields in both forms, matching your project needs. Each field = column in `component_people` database table. It is highly recommended to take a look at database table structure first and see what columns are there (for example `twitter_link`, `linkedin_link` etc.). 

If you need more fields than those that are available by default, jump to **Adding new columns...** chapter below in this tutorial.

As soon as you have prepared the forms and added a few people, now it is time to display them on the frontend.

### Step 3
Create a new section blade template and JSON config file. Below you can find an example of a section and its fields - of course feel free to configure it as your project demands.

Example of `cms-backend/resources/views/Website/Sections/section-meet-the-team.blade.php`:

```
<section 
    class="
        section
        @include('Website.Sections.columns-sections-partials.section-settings')
    "
    @include('Website.Templates.section-anchor-id')>

    <div class="@include('Website.Sections.columns-sections-partials.container-settings')">
        @if (!empty($components->headline))
            <h2 
                class="
                    headline-2
                    {!! HTML::fontColour($components, 'headline_colour') !!}
                ">
                {{ $components->headline }}
            </h2>
        @endif

        @if (!empty($components->people_component->id))
            {!! \WebsiteComponent::people(
                $components->people_component->id, 
                'meet-the-team'
            ) !!}
        @endif
    </div>
</section>
```

***Attention!*** The most important part of the code above is the following one:

```
    {!! \WebsiteComponent::people(
        $components->people_component->id, 
        'meet-the-team',
        [
            'someData' => 'someValue
        ]
    ) !!}
```

This method will return the template with the people added in that component in Alfred.

In the **first parameter**, there is just the ID of the component selected in Alfred.

The **second parameter** represents the name of component blade template that will be included over there. In the step 4 you will find out what it is.

In the **third param** (optional) you can pass an array of items. The items under the indexes will be available in the component template as variables (for example ```$someData```).

Example of `cms-backend/resources/views/Website/Sections/jsons/section-meet-the-team.json`:

```
{
    "name": "Meet the team",
    "type": [
        "page",
        "homepage"
    ],
    "group": "types",
    "components": {
        "settings": {
            "order": 0,
            "type": "section-modal",
            "modal_type": "section-settings",
            "label": "Settings"
        },
        "headline": {
            "order": 1,
            "type": "input",
            "label": "Headline",
            "cms_width": 8
        },
        "headline_colour": {
            "order": 3,
            "type": "text-colours",
            "label": "Headline colour",
            "cms_width": 4
        },
        "people_component": {
            "order": 4,
            "type": "custom-component",
            "label": "Assign component",
            "options": [
                {
                    "label": "Choose component"
                },
                {
                    "label": "People",
                    "value": "people"
                }
            ]
        },
        "section_anchor": {
            "order": 5,
            "label": "Section anchor",
            "type": "input"
        }
    }
}

```

### Step 4
Now you have to create a component blade template responsible for displaying only the people's component records (people). 

Create a file in the `cms-backend/resources/views/Website/Components/` folder under the name passed in the 2nd parameter of ```\WebsiteComponent::people()``` method - for example `cms-backend/resources/views/Website/Components/meet-the-team.blade.php`. 

Below you can find an example of the component template - feel free to adjust it as you want.

```
@if (!empty($people) && $people->isNotEmpty())

    @foreach ($people as $person)
        @if (!empty($person->photo))
            <figure>
                {!! Imgix::imageCustom($person, 'photo', 'name', 'person', 'image', 'image') !!}
            </figure>
        @endif

        {{ $person->name }}

        @if (!empty($person->description))
            <p>
                {!! nl2br($person->description) !!}
            </p>
        @endif
    @endforeach

@endif
```

### Step 5
Go to Alfred, add a section using the new section type, add some people and that's it!

---

## Adding new columns in the `component_people` database table (**optional**)
If your project requires totally different fields than those available by default, create a database migration and add the columns you want.

Once the columns are created, you need to remember to:

- specify the columns that should be fillable in `cms-backend/app/Cms/Modules/Components/Entities/People.php`;
- add the fields to `cms-backend/cmsassets/_ng/Components/people-create.tpl.html` and `cms-backend/cmsassets/_ng/Components/people-edit.tpl.html`;
- specify the new columns in `cms-backend/app/Website/Repositories/ComponentPeopleRepositoryEloquent.php` in the ```get()``` method;
- Run `php artisan cache:clear` from the `cms-backend/` folder (because all the records in components are cached by default).

---

## Opening person's bio/info in a popup (**optional**)
### Step 1
Create a new route:

```php
Route::get('ajax/person-details/{id}', ['as' => 'website.person-details', 'uses' => 'Website\Controllers\AjaxController@getPersonDetails']);
```

### Step 2
In the `app/Website/Controllers/AjaxController.php` create a new method:

```php
public function getPersonDetails(int $id)
{
    return $this->componentService->getPersonDetailsView($id);
}
```

### Step 3
In `cms-backend/app/Website/Services/ComponentService.php` add a new method:

```php
public function getPersonDetailsView(int $personId)
{
    $person = $this->peopleRepo->getSinglePerson($personId);

    if (!$person) {
        abort(404);
    }

    return view('Website.Components.single-person-details', compact('person'));
}
```

### Step 4
In `cms-backend/app/Website/Repositories/ComponentPeopleRepositoryEloquent.php` create a new method:

```php
public function getSinglePerson(int $personId)
{
    return $this->model
        ->where('id', '=', $personId)
        ->get(['id', 'name', 'position', 'photo', 'email', 'linkedin_link', 'description'])
        ->first();
}
```

Of course you can specify other needed columns to return over there.

### Step 5
In `cms-backend/app/Website/Repositories/ComponentPeopleInterface.php` specify the same method:

```php
public function getSinglePerson(int $personId);
```

### Step 6
Create the blade template in `cms-backend/resources/views/Website/Components/single-person-details.blade.php` responsible for the template visible in a popup. Below you can find an example of the code:

```
<button class="popup__close" type="button" onclick="App.closePopup()" title="Close popup">
    <svg width="24px" height="24px" class="popup__close-icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/images/icons.svg#icon-close"></use>
    </svg>
</button>

<div class="popup-inner popup-inner--two-column">
    @if (!empty($person->photo))
        <div class="popup-inner__column">
            <figure class="popup-inner__image image-wrapper">
                {!! Imgix::imageCustom($person, 'photo', 'name', 'semi-full-width-image', 'image', 'image-no-lazy') !!}
            </figure>
        </div>
    @endif

    <div class="popup-inner__column popup-inner__content">
        <h1 class="popup-inner__headline headline-2">
            {{ $person->name }}
        </h1>

        @if (!empty($person->description))
            <div class="popup-inner__copy content">
                <p>
                    {!! nl2br($person->description) !!}
                </p>
            </div>
        @endif

        <div class="popup-inner__button-wrapper">
            @if (!empty($person->email))
                <a 
                    href="{!! HTML::encode('mailto:' . $person->email) !!}" 
                    title="Email me" 
                    class="btn btn--primary btn--green-white">
                    <span>Email me</span>
                </a>
            @endif

            @if (!empty($person->linkedin_link))
                <a 
                    href="{{ $person->linkedin_link }}" 
                    class="social-link" 
                    target="_blank" 
                    rel="noopener noreferrer"
                    title="My LinkedIn profile">
                    <svg class="social-link__icon fill-white" width="54" height="54" aria-hidden="true">
                        <use
                            xmlns:xlink="http://www.w3.org/1999/xlink" 
                            href="/images/icons.svg#icon-linkedin">
                        </use>
                    </svg>
                </a>
            @endif
        </div>
    </div>
</div>
```

Feel free to adjust it.

### Step 7
Now you need to create frontend (JS) code responsible for opening a popup where the content will come from the response of the XHR request to `GET ajax/person-details/{id}` URL.

You can link up each person (return URL to the route dynamically) as below:

```php
{{ route('website.person-details', ['id' => $person->id]) }}
```