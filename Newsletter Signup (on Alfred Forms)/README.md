# Newsletter Signup (on Alfred Forms)
---

## Introduction
There might be a case when there is no 3rd party newsletter integration and simple signup to Newsletter list relied on Alfred Forms module is needed. Here you can find a full process of making this happen in a few simple steps!

![Example of implementation](https://docs.alfred-cms.com/alfred-forms-newsletter-signup.png)

![Newsletter form submission listing](https://docs.alfred-cms.com/alfred-forms-newsletter-signup2.png)

![Newsletter form submission view](https://docs.alfred-cms.com/alfred-forms-newsletter-signup3.png)

---

## What you need to do
1.  Go to Alfred --> Forms module and create a new form (change its name to Newsletter in the General tab). Delete all created by default fields and keep just one - Email (with type Email set + with Required setting switched on).
2. As the Newsletter Form signup will be relied on this form and to avoid unnecessary issues, it is recommended to block this form from further edits or accidental removal. Therefore please edit the `cms-backend/resources/cmsassets/_ng/Forms/forms-management.tpl.html` and change the code responsible for the forms listing as below:

```
    <tr ng-repeat="form in management.list.data">
        <td class="table__narrow-column">{{form.id}}</td>
        <td>
            <!-- Only forms with different than 'Newsletter' name are available to edit -->
            <a 
                ng-if="form.name !== 'Newsletter'"
                ui-sref="forms.management-single({formId: form.id})" 
                href>
                <b>{{form.name}}</b>
            </a>
            <b ng-if="form.name === 'Newsletter'">
                {{form.name}}
            </b>
        </td>
        <td class="text-right">
            <!-- Delete button is disabled for a form with 'Newsletter' name defined -->
            <button 
                type="button"
                ng-disabled="form.name === 'Newsletter'"
                class="btn btn-danger btn-xs" 
                ng-click="management.remove(form)" 
                >
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </td>
    </tr>
```

This way the form with *Newsletter* name will not be editable or removeable by Alfred user.

3. Create `cms-backend/config/alfred-forms.php` file with following configuration (please adjust the ID of the form following your Newsletter form ID):

```
    <?php

    return [
        'newsletter' => [
            'id' => 2,
            'name' => 'Newsletter'
        ]
    ];
```

4. Next step is to create a dedicated route. Go edit the `cms-backend/app/Http/routes.php` and:

- declare `use Website\Modules\Forms\Controllers\FormController;` at the top of the file
- add route such as  `Route::post('ajax/newsletterSubscribeForm', [FormController::class, 'newsletterSubscribeForm'])->name('website.subscribeForm');` somewhere under the **Website functionalities** comment over there.

5. In `cms-backend/app/Website/Modules/Forms/Controllers/FormController.php` create a method such as:

```
    public function newsletterSubscribeForm(FormNewsletterRequest $request)
    {
        $recaptchaError = $this->validateRecaptcha($request->get('recaptcha'));

        if ($recaptchaError) {
            return $recaptchaError;
        }

        $email = $request->get('email');

        if (FormSubmission::where([
            'customer_email' => $email,
            'form_name' => config('alfred-forms.newsletter.name')
        ])->exists()) {
            return response()->json(['error' => true, 'message' => 'Your email is already signed up.'], 400);
        }

        $data = [
            'customer_name' => '',
            'customer_email' => $email,
            'all_fields_from_form' => [
                ['display_name' => 'Email', 'value' => $email]
            ],
            'form_name' => config('alfred-forms.newsletter.name'),
            'form_id' => config('alfred-forms.newsletter.id'),
            'uploaded_file' => null,
        ];

        FormSubmission::create($data);

        return response()->json(['error' => false, 'message' => 'Thank you for your subscription.']);
    }
```

At the top of the `FormController.php` please declare:
```
use Website\Modules\Forms\Requests\FormSubmissionRequest;
```

Create `cms-backend/app/Website/Modules/Forms/Requests/FormNewsletterRequest.php` with following content:

```
    <?php

    namespace Website\Modules\Forms\Requests;

    use App\Http\Requests\Request;

    class FormNewsletterRequest extends Request
    {
        /**
        * Determine if the user is authorized to make this request.
        *
        * @return bool
        */
        public function authorize()
        {
            return true;
        }

        /**
        * Get the validation rules that apply to the request.
        *
        * @return array
        */
        public function rules()
        {
            return [
                'email' => 'required',
            ];
        }
    }

```

Done! Now you need to take care of the frontend (if it's not ready yet).

## Frontend
Next step is to create a form on the frontend page that should send the entered email address by user in the payload to the API endpoint `ajax/newsletterSubscribeForm`. By default this form needs to support Google reCaptcha that should be prepared by front-end developer.

Example of front-end template of such newsletter signup form:

```
    <form 
        id="newsletter" 
        class="form" 
        onsubmit="return App.submitForm({form: this, url: '/ajax/newsletterSubscribeForm'});">
        @csrf
        <input type="hidden" name="recaptcha">
        
        <div class="form__row">
            <div class="form__group">
                <input class="form__input" type="email" name="email" required>
                <label for="email" class="form__label">Email</label>
            </div>
        </div>

        <button type="submit">
            Submit
        </button>

        <div class="loading"></div>
    </form>
```