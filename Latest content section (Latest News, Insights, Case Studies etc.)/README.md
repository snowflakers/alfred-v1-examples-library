# Latest Content section (for example Latest News / Blog / Insights / Case Studies / Products etc.)
---

## Introduction
Almost every project has a section like Latest News or Latest Insights etc. See a few screen shots of it to realise what this tutorial is about.

![Example of Latest News section](https://docs.alfred-cms.com/section-latest-content-example1.png)

![Example of Latest Blog section](https://docs.alfred-cms.com/section-latest-content-example2.png)

![Example of Latest Insights section](https://docs.alfred-cms.com/section-latest-content-example3.png)

---

## Solution
Below you can find an easy tutorial how to implement this kind of section in your Alfred-based project.

The section, from administrative point of view, gives an ability to choose a category of module where the latest pieces of content come from and - additionally - to override them if there is a need for it. Also, it is possible to display a featured piece of content on top of it. Of course, feel free to remove it if you do not want it.

---

## What you need to do

Let's assume you are going to implement a section called **Latest Insights**. If you want to create the same but for a different module such as Blog, News, Products etc. - feel free to just change the `insights` mention to your desired module.

### Step 1
Create an empty (for now) section file: `cms-backend/resources/views/Website/Sections/section-latest-insights.blade.php`.

### Step 2
Create a section configuration file in `cms-backend/resources/views/Website/Sections/jsons/section-latest-insights.json`:

```
{
    "name": "Latest insights",
    "type": [
        "page",
        "homepage"
    ],
    "group": "special",
    "components": {
        "settings": {
            "order": 1,
            "type": "section-modal",
            "modal_type": "section-settings",
            "label": "Settings"
        },
        "headline": {
            "order": 2,
            "type": "input",
            "label": "Headline"
        },
        "category": {
            "order": 3,
            "type": "include-filter",
            "label": "Category from Insights module",
            "search_type": "insights-category"
        },
        "overrides": {
            "order": 4,
            "type": "overwrite-pages",
            "label": "Overriden pages",
            "search_type": "insights",
            "limit": 4
        },
        "no_featured_page": {
            "order": 5,
            "type": "checkbox",
            "label": "No featured insight visible (only 3 latest insights will be displayed)"
        },
        "section_anchor": {
            "order": 6,
            "label": "Section anchor",
            "type": "input"
        }
    }
}
```

Of course, if you need more fields - just add them!

Now, if you go to Alfred, you should be able to choose that section type and it should look as below:

![Example of Latest Insights section in Alfred](https://docs.alfred-cms.com/section-latest-content-alfred-preview.png)

In the Category field, you have to choose a module category page where the pieces of content will come from. It can be just the main page of the Insights module but also some category page from Insights.


#### Search filter
Either in **category** or **overrides** field you can find `"search_type": "insights"` that is responsible for filtering the Alfred pages by module and action. `insights-category` will return only insight category pages and `insights` will display only single insight pages.

These search types are defined in `cms-backend/app/Cms/Modules/Pages/Repositories/PagesRepositoryEloquent.php` - `search` method. You will find there a ```switch ($type)``` where you can define more cases.


### Step 3
Let's go back to the front-end blade template file - `cms-backend/resources/views/Website/Sections/section-latest-insights.blade.php`. Below you can find an example of the front-end code with implementation. Of course, feel free to adjust it to your needs.

```
<section 
    class="
        section 
        @include('Website.Sections.columns-sections-partials.section-settings')
    "
    @include('Website.Templates.section-anchor-id')>
    
    @include('Website.Sections.columns-sections-partials.background')

    @include('Website.Sections.columns-sections-partials.overlay')

    <div class="@include('Website.Sections.columns-sections-partials.container-settings')">
        @if (!empty($components->headline))
            <h2 class="headline-2">
                {{ $components->headline }}
            </h2>
        @endif

        @php
            $numberOfPages = 4;
            $selectedPagesCollection = \WebsiteBase::getMorePagesFromCategoryWithOverriddenOption($components, 'overrides', 'insights', $numberOfPages);

            if ($selectedPagesCollection->count() < $numberOfPages) {
                $categoryId = !empty($components->category->id) ? $components->category->id : WebsiteBase::getMainModulePage('insights')['id'];
                $categoryPage = \WebsiteBase::getCategoryWithLatestItems($categoryId, ($numberOfPages - $selectedPagesCollection->count()));

                if (!empty($categoryPage->singlePages)) {
                    $selectedPagesCollection = $selectedPagesCollection->merge($categoryPage->singlePages);
                }
            }
        @endphp

        @if (!empty($selectedPagesCollection) && !$selectedPagesCollection->isEmpty())
            @if (empty($components->no_featured_page))
                @include(
                    'Website.Insights.category.partials.single-featured-insight-tile', 
                    ['singlePage' => $selectedPagesCollection[0]]
                )

                @if (count($selectedPagesCollection) > 1)
                    <div 
                        class="row row--g-20 align-items-stretch">
                        @foreach ($selectedPagesCollection->slice(1) as $singlePage)
                            @include('Website.Insights.category.partials.single-insight-tile')
                        @endforeach
                    </div>
                @endif
            @else
                <div class="row row--g-20 align-items-stretch">
                    @foreach ($selectedPagesCollection->slice(0, 3) as $singlePage)
                        @include('Website.Insights.category.partials.single-insight-tile')
                    @endforeach
                </div>
            @endif
        @endif
    </div>
</section>
```

The above example presents **a case of a featured insight at the top**. If you do not need it - try the code snippet below:

```
<section 
    class="
        section 
        @include('Website.Sections.columns-sections-partials.section-settings')
    "
    @include('Website.Templates.section-anchor-id')>
    
    @include('Website.Sections.columns-sections-partials.background')

    @include('Website.Sections.columns-sections-partials.overlay')

    <div class="@include('Website.Sections.columns-sections-partials.container-settings')">
        @if (!empty($components->headline))
            <h2 
                class="headline-2 mb-0em">
                {{ $components->headline }}
            </h2>
        @endif

        @php
            $numberOfPages = 4;
            $selectedPagesCollection = \WebsiteBase::getMorePagesFromCategoryWithOverriddenOption($components, 'overrides', 'insights', $numberOfPages);

            if ($selectedPagesCollection->count() < $numberOfPages) {
                $categoryId = !empty($components->category->id) ? $components->category->id : WebsiteBase::getMainModulePage('insights')['id'];
                $categoryPage = \WebsiteBase::getCategoryWithLatestItems($categoryId, ($numberOfPages - $selectedPagesCollection->count()));

                if (!empty($categoryPage->singlePages)) {
                    $selectedPagesCollection = $selectedPagesCollection->merge($categoryPage->singlePages);
                }
            }
        @endphp

        @if (!empty($selectedPagesCollection) && !$selectedPagesCollection->isEmpty())
            <div class="row row--g-20 align-items-stretch">
                @foreach ($selectedPagesCollection->slice(0, 3) as $singlePage)
                    @include('Website.Insights.category.partials.single-insight-tile')
                @endforeach
            </div>
        @endif
    </div>
</section>
```

**Attenton:**
Quick explanation of this line of code:

```
$selectedPages = \WebsiteBase::getMorePagesFromCategoryWithOverriddenOption($components, 'overrides', 'insights', $numberOfPages);
```

- 2nd parameter is the field name from your JSON file that represents the overriden pages. Do not change it unless you change it also in the JSON file.
- 3rd parameter represents the module name so in case of a different module - just change this name.
- 4th parameter is the number of pages returned by this function. All depends how many pieces of content you want to display in your section.

Go to the next (and the last) step below.

### Step 4
Now you need to create a blade template for the single block (following the paths above), examples:

- `cms-backend/resources/views/Insights/category/partials/single-insight-tile.blade.php`
- if you use the featured block at the top: `cms-backend/resources/views/Insights/category/partials/single-featured-insight-tile.php`.

Example of the code within that single block/tile template:

```
@if (!empty($singlePage))
    @php
        if (!empty($singlePage['lead'])) {
            $singlePage['lead'] = (array) $singlePage['lead'];
        }
    @endphp

    <article class="col-md-6 col-lg-4">
        <a 
            href="{{ $root . $singlePage['slug'] }}" 
            title="{{ $singlePage['title'] }}">

            @if (!empty($singlePage['lead']['image']))
                <figure>
                    {!! Imgix::imageCustom(
                        $singlePage['lead'], 
                        'image', 
                        'image_alt', 
                        'semi-full-width-image', 
                        null, 
                        'image'
                    ) !!}
                </figure>
            @endif

            <h2>
                {{ $singlePage['title'] }}
            </h2>

            @if (!empty($singlePage['lead']['text']))
                <p>
                    {!! nl2br($singlePage['lead']['text']) !!}
                </p>
            @endif
        </a>
    </article>
@endif
```

That's it!